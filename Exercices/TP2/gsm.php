<?php
    //require_once "../../Helpers/tp2-helpers.php";

    $gsm_csv = file("../../Donnees/gsm-csv.csv"); // On récupère le fichier CSV au format CSV
    array_shift($gsm);

    // --- 1. --- //
    echo "Nombre de points d'accès : ".(count($gsm_csv) - 1); // Nombre de points d'accès (moins l'en-tête)

    // --- 2. --- //
    $nb_operateurs = ["BYG" => 0, "SFR" => 0, "ORA" => 0, "FREE" => 0];
    foreach($gsm_csv as $ligne)
    {
        $donnees = str_getcsv($ligne, ";");
        switch ($donnees[3])
        {
            case "BYG":
                $nb_operateurs["BYG"]++;
                break;
            case "SFR":
                $nb_operateurs["SFR"]++;
                break;
            case "ORA":
                $nb_operateurs["ORA"]++;
                break;
            case "FREE":
                $nb_operateurs["FREE"]++;
                break;
            default:
                break;
        }
    }
    echo "\n\nNombre d'opérateurs :\n\tBouygues : ".$nb_operateurs["BYG"]."\n\tSFR : ".$nb_operateurs["SFR"]."\n\tOrange : ".$nb_operateurs["ORA"]."\n\tFree : ".$nb_operateurs["FREE"]."\n";
?>
