<html>

<head>
    <title>GSM</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
</head>

<body>
    <form method="get" action="gsm-webservice.php">
        <label for="lat">Latitude :</label>
        <input type="text" id="lat" name="lat" value="45.19102", required />
        <br>
        <label for="lon">Longitude :</label>
        <input type="text" id="lon" name="lon" value="5.72752" required />
        <br>
        <label for="lon">Opérateur :</label>
        <select id="ope" name="ope" required >
            <option value="BYG">Bouygues</option>
            <option value="SFR">SFR</option>
            <option value="ORA">Orange</option>
            <option value="FREE">Free</option>
        </select>
        <br>
        <label for="top">Top :</label>
        <input type="text" id="top" name="top" value="10" required />
        <br><br>
        <input type="submit" name="src" value="CSV" />
        <input type="submit" name="src" value="JSON" />
    </form>
</body>

</html>

<style> <?php include "tab.css" ?> </style>

<?php
    session_start();
    if(!isset($_SESSION["res"]))
        $_SESSION["res"] = NULL;
    $res = $_SESSION["res"];

    if($res != NULL)
    {
        $res_decoded = json_decode($res, TRUE);
        echo "<table><thead><th>ID</th><th>Adresse</th><th>Opérateur</th><th>Latitude</th><th>Longitude</th><th>Distance</th><tbody><tr>";
        foreach($res_decoded as $row)
            echo "<td>".$row["id"]."</td><td>".$row["adr"]."</td><td>".$row["ope"]."</td><td>".$row["lat"]."</td><td>".$row["lon"]."</td><td>".$row["dist"]."</td></tr><tr>";
        echo "</tbody></table>";
    }
?>