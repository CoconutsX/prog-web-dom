% PW-DOM  Compte rendu de TP

# Compte-rendu de TP

Sujet choisi : Données ouvertes de la ville de Grenoble

## Participants 

* CHIOTTI Maël
* CAILLES Maxime
* CANIN Corentin

## Filtres Unix

### Visualisation texte

  `cat Donnees/wifi-csv.csv` et `cat Donnees/wifi-json.json` affichent le contenu des fichiers.  
  `file Donnees/wifi-json.json` et `file Donnees/wifi-csv.csv` nous indiquent l'encodage des fichiers, respectivement `JSON data` et `CSV text`.

### Comptage 

  On peut utiliser la commande `wc` avec l'option `-l` pour compter le nombre de lignes : `wc -l wifi-csv.csv`.  
  Il y a 69 lignes donc 69 points d'accès.

### Points Multiples

  La commande `cut -d, -f2 *.csv | uniq -c | sort -r` permet de découper le fichier avec les virgules qu'il contient (`cut`). On regarde la deuxième "colonne" pour regarder le nom du lieu contenant chaque antenne dont on compte les occurences (`uniq`) et qu'on range dans l'ordre décroissant (`sort`).  
  Il y a 59 points uniques et le lieu avec le plus de points est la *Bibliothèque des Etudes* qui en a 5.

## Traitements PHP

  **Voir :** `wifi.php`  
  ⚠️ Toutes les exécutions CLI doivent se faire depuis le dossier *Exercices/TP2*

### Comptage PHP

  On convertit le fichier texte CSV en tableau grâce à la fonction `file`, puis on affiche le resultat de la fonction `count()` qui compte le nombre de lignes.

### Structure de données

  On utilise le fichier récupéré plus tôt avec `file`, et on en extrait chaque ligne que l'on traite avec `str_getcsv`. On obtient ainsi pour chaque antenne un tableau de 4 cases qu'on renvoit ensuite dans un dictionnaire à quatre champs : "name", "adr", "lon" et "lat". Ce dernier est ajouté dans un autre tableau contenant toutes les données.  
  Au cours de la question 4 nous avons décidé, afin de nous faciliter la tâche, de rajouter un champ "dist" à chaque dictionnaire. Il correspond à la distance entre le point et la Place Grenette.

### Proximité

  On crée un point correspondant aux coordonnées de la Place Grenette. On parcourt notre tableaux de points précédemment créé et on ajoute dans un tableau chaque point dont la distance avec la place Grenette est inférieure à 200m (calculée avec la fonction `distance()` fournie). On affiche ensuite ce tableau, ce qui nous indique qu'il y a donc 7 points à moins de 200m de la Place Grenette. Le plus proche est *(étonnament...)* Place Grenette à 20.1m.

### Proximité top N

  On trie le tableau contenant tous les points par la distance (puis le nom) grâce à la fonction `array_multisort`. On affiche les N premières cases, N étant entré en paramètre du programme par l'utilisateur.

### Géocodage

  On utilise la fonction `smartcurl()` fournie afin d'interroger l'API d'adresses à partir le la longitude et de la latitude de chacun des points. L'API renvoie des données au format JSON, que nous décodons à l'aide de la fonction `json_decode()`. Nous vérifions enfin que l'API n'a pas renvoyé une erreur (ce qui arrive pour la première ligne du fichier CSV car les coordonnées sont du texte), avant d'afficher le champs "name" qui correspond à l'adresse (numéro de voie et voie).

**Voir :**  
`webservice.php`  
`client.php`

### Webservice | Format JSON | Client webservice

  Du côté du webservice, les données sont lues à l'aide de la variable superglobale `$GET_`. En plus de `lat`, `lon` et `top`, nous lisons aussi `src`, qui nous indique si l'utilisateur souhaite utiliser le jeu de données au format CSV ou au format GEOJSON.  
  Pour chaque cas, le fichier est décodé, traité, trié puis les résultats sont extraits. Une variable de session `res` est utilisée pour pouvoir transmettre le résultat à la page du client.  
  Dans le cas du GEOJSON, la fonction `file_get_contents()` est préférée à `file()` afin d'obtenir le contenu du fichier sous la forme d'une chaîne de caractère et non d'un tableau, ce qui est ensuite utile pour utiliser la fonction `json_decode()`. Celle-ci possède l'argument `$assoc = TRUE` afin que les résultats soient renvoyés sous la forme de tableaux associatifs et non d'objets.  
  Du côté du client, le résultat au format JSON est décodé pour ensuite être représenté sous la forme d'une table.

---

## Antennes GSM

### CSV Antennes

  Il y a 100 antennes (il ne faut pas oublier, lors du comptage de lignes avec `count()`, de retirer la première qui correspond à l'en-tête du fichier CSV).  
  Ce jeu de fichiers contient des informations supplémentaires comme la technologie utilisée par les antennes (2G/3G/4G) ou l'opérateur ayant installé l'antenne. De plus, il possède plusieurs identifiants (par antenne, pour l'adresse, le support...).  
  Ces données sont donc complètes et les identifiants permettent de les recroiser avec d'autres jeux, ce qui est utile dans une démarche opendata.

### Statistiques opérateurs

  On sait qu'il y a 4 opérateurs en France. Il nous suffit donc de créer 4 compteurs qui dénombreront leur nombre d'antennes respectifs dans le jeu de données. Nous avons décidé de les regrouper dans un tableau associatif, mais 4 variables pourrait aussi fonctionner. Une structure `switch` permet ensuite, pour chaque antenne, d'incrémenter le compteur correspondant à son opérateur. Cela nous semble être la méthode la plus simple et la plus direct, car elle ne requiert qu'un seul parcourt de la liste des antennes.

### KML validation

  TODO

### KML bis

  Le fichier au format KML est de toute évidence très difficile à lire pour un humain. Il utilise beaucoup plus de lignes et de place que les autres formats, et semble posséder une redondance des informations (en double).

### Top N opérateur

  Une condition est ajoutée pour la sélection des antennes GSM : elles doivent appartenir à un opérateur en particulier. Pour cela, la boucle `for` de sélection des résultats va itérer sur deux valeurs : `$i` qui va servir à parcourir tout le tableau de données, et `$j` qui va arrêter la boucle dès que `$top` résultats ont été sélectionnés.