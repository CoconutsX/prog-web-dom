<?php
    require_once "../../Helpers/tp2-helpers.php";

    $wifi_csv = file("../../Donnees/wifi-csv.csv"); // On récupère le fichier CSV au format CSV

    // --- 3. --- //
    echo "Nombre de points d'accès : ".count($wifi_csv); // Nombre de points d'accès

    // --- 4. --- //
    $coord_grenette = ["lon" => "5.72752", "lat" => "45.19102"];
    $tab_data = [];

    foreach($wifi_csv as $ligne) {
        $donnees = str_getcsv($ligne);

        $dico_tmp = [];
        $dico_tmp["name"] = $donnees[0];
        $dico_tmp["adr"] = $donnees[1];
        $dico_tmp["lon"] = $donnees[2];
        $dico_tmp["lat"] = $donnees[3];
        $dico_tmp["dist"] = distance($coord_grenette, $dico_tmp);
        
        array_push($tab_data, $dico_tmp);
    }

    // --- 5. --- //
    echo "\n\nPoints à moins de 200m de la Place Grenette :\n";
    foreach($tab_data as $point) {
        $distance = distance($coord_grenette, $point); // Calcul de la distance entre la place Grenette et le point
        if($distance < 200) {
            echo $point["name"].", dist : ".$distance."m, lat : ".$point["lat"].", lon : ".$point["lon"]."\n";
        }
    }

    // --- 6. --- //
    $N = $argv[1];
    echo "\n".$N." points les plus proches de la Place Grenette :\n";
    $names = array_column($tab_data, "name"); // Colomne des noms
    $dists = array_column($tab_data, "dist"); // Colomne des distances
    array_multisort($dists, SORT_ASC, $names, SORT_ASC, $tab_data); // Tri ascendant sur les colonnes distances puis noms
    for($i = 0; $i < $N && $i < count($tab_data); $i++)
        echo $tab_data[$i]["name"].", dist : ".$tab_data[$i]["dist"]."m, lat : ".$tab_data[$i]["lat"].", lon : ".$tab_data[$i]["lon"]."\n";

    // --- 7. --- //
    echo "\nEmplacement des points :\n";
    foreach($tab_data as $point) {
        $geocodage_json = smartcurl("https://api-adresse.data.gouv.fr/reverse/?lon=".$point["lon"]."&lat=".$point["lat"], 0); // JSON renvoyé par l'API
        $geocodage = json_decode($geocodage_json, true); // Décodage du JSON
        
        if(!(array_key_exists("title", $geocodage) && $geocodage["title"] == "Invalid args")) // Vérification que l'API n'a pas renvoyé une erreur d'arguments (1ère ligne du CSV)
            echo $point["name"]." se situe à : ".$geocodage["features"][0]["properties"]["name"]."\n";
    }
?>
