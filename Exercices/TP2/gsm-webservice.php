<?php
    require_once "../../Helpers/tp2-helpers.php";

    session_start();
    if(!isset($_SESSION["res"]))
        $_SESSION["res"] = NULL;
    $res = $_SESSION["res"];

    $lat = isset($_GET["lat"]) ? $_GET["lat"] : NULL;
    $lon = isset($_GET["lon"]) ? $_GET["lon"] : NULL;
    $top = isset($_GET["top"]) ? $_GET["top"] : NULL;

    if(isset($_GET["src"]) && $_GET["src"] == "CSV")
        $src = $_GET["src"];
    else if(isset($_GET["src"]) && $_GET["src"] == "JSON")
        $src = $_GET["src"];
    else
        $src = NULL;

    if(isset($_GET["ope"]))
    {
        switch ($_GET["ope"]) {
            case "BYG":
                $ope = "BYG";
                break;
            case "SFR":
                $ope = "SFR";
                break;
            case "ORA":
                $ope = "ORA";
                break;
            case "FREE":
                $ope = "FREE";
                break;
            default:
                $ope = NULL;
                break;
        }
    }
    else
        $ope = NULL;

    if($lat != NULL && $lon != NULL && $top != NULL && $src != NULL && $ope != NULL)
    {
        $tab_data = [];
        $coord = ["lon" => $lon, "lat" => $lat];
        $tab_res = [];

        // Données CSV
        if($src == "CSV")
        {
            $gsm = file("../../Donnees/gsm-csv.csv");
            array_shift($gsm);

            // Traitement
            foreach($gsm as $ligne)
            {
                $donnees = str_getcsv($ligne, ";");

                $dico_tmp = [];
                $dico_tmp["id"] = $donnees[0];
                $dico_tmp["adr"] = $donnees[7];
                $dico_tmp["ope"] = $ligne[3];
                $dico_tmp["lon"] = $donnees[5];
                $dico_tmp["lat"] = $donnees[6];
                $dico_tmp["dist"] = distance($coord, $dico_tmp);
                
                array_push($tab_data, $dico_tmp);
            }
        }

        // Données GEOJSON
        else
        {
            $gsm = file_get_contents("../../Donnees/gsm-json.json");
            $gsm_decoded = json_decode($gsm, TRUE)["features"];

            // Traitement
            foreach($gsm_decoded as $ligne)
            {
                $dico_tmp = [];
                $dico_tmp["id"] = $ligne["properties"]["ANT_ID"];
                $dico_tmp["adr"] = $ligne["properties"]["ANT_ADRES_LIBEL"];
                $dico_tmp["ope"] = $ligne["properties"]["OPERATEUR"];
                $dico_tmp["lon"] = $ligne["geometry"]["coordinates"][0];
                $dico_tmp["lat"] = $ligne["geometry"]["coordinates"][1];
                $dico_tmp["dist"] = distance($coord, $dico_tmp);
                
                array_push($tab_data, $dico_tmp);
            }
        }

        // Tri
        $names = array_column($tab_data, "id"); // Colomne des noms
        $dists = array_column($tab_data, "dist"); // Colomne des distances
        array_multisort($dists, SORT_ASC, $names, SORT_ASC, $tab_data); // Tri ascendant sur les colonnes distances puis noms

        // Résultat
        for($i = 0, $j = 0; $i < count($tab_data) && $j < $top; $i++)
        {
            if($tab_data[$i]["ope"] == $ope)
            {
                array_push($tab_res, $tab_data[$i]);
                $j++;
            }
        }

        // Retour
        $res = json_encode($tab_res);
        $_SESSION["res"] = $res;
    }
    else
        $_SESSION["res"] = NULL;

    header('Location: gsm-client.php'); // Redirection
?>
