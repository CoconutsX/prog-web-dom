<?php
    require_once "../../Helpers/tp2-helpers.php";

    session_start();
    if(!isset($_SESSION["res"]))
        $_SESSION["res"] = NULL;
    $res = $_SESSION["res"];

    $lat = isset($_GET["lat"]) ? $_GET["lat"] : NULL;
    $lon = isset($_GET["lon"]) ? $_GET["lon"] : NULL;
    $top = isset($_GET["top"]) ? $_GET["top"] : NULL;

    if(isset($_GET["src"]) && $_GET["src"] == "CSV")
        $src = $_GET["src"];
    else if(isset($_GET["src"]) && $_GET["src"] == "JSON")
        $src = $_GET["src"];
    else
        $src = NULL;

    if($lat != NULL && $lon != NULL && $top != NULL && $src != NULL)
    {
        $tab_data = [];
        $coord = ["lon" => $lon, "lat" => $lat];
        $tab_res = [];

        // Données CSV
        if($src == "CSV")
        {
            $wifi = file("../../Donnees/wifi-csv.csv");

            // Traitement
            foreach($wifi as $ligne)
            {
                $donnees = str_getcsv($ligne);

                $dico_tmp = [];
                $dico_tmp["name"] = $donnees[0];
                $dico_tmp["adr"] = $donnees[1];
                $dico_tmp["lon"] = $donnees[2];
                $dico_tmp["lat"] = $donnees[3];
                $dico_tmp["dist"] = distance($coord, $dico_tmp);
                
                array_push($tab_data, $dico_tmp);
            }
        }

        // Données GEOJSON
        else
        {
            $wifi = file_get_contents("../../Donnees/wifi-json.json");
            $wifi_decoded = json_decode($wifi, TRUE)["features"];

            // Traitement
            foreach($wifi_decoded as $ligne)
            {
                $dico_tmp = [];
                $dico_tmp["name"] = $ligne["properties"]["AP_ANTENNE1"];
                $dico_tmp["adr"] = $ligne["properties"]["Antenne 1"];
                $dico_tmp["lon"] = $ligne["properties"]["longitude"];
                $dico_tmp["lat"] = $ligne["properties"]["latitude"];
                $dico_tmp["dist"] = distance($coord, $dico_tmp);
                
                array_push($tab_data, $dico_tmp);
            }
        }

        // Tri
        $names = array_column($tab_data, "name"); // Colomne des noms
        $dists = array_column($tab_data, "dist"); // Colomne des distances
        array_multisort($dists, SORT_ASC, $names, SORT_ASC, $tab_data); // Tri ascendant sur les colonnes distances puis noms

        // Résultat
        for($i = 0; $i < $top; $i++)
            array_push($tab_res, $tab_data[$i]);

        // Retour
        $res = json_encode($tab_res);
        $_SESSION["res"] = $res;
    }
    else
        $_SESSION["res"] = NULL;

    header('Location: wifi-client.php'); // Redirection
?>
