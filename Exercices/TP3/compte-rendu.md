% PW-DOM  Compte rendu de TP 3

# Compte-rendu de TP

Sujet choisi : TMDB

## Participants 

* CAILLES Maxime
* CANIN Corentin
* CHIOTTI Maël

## Utilisation de The Movie Database

### Mise en jambes

**Voir :** `movie_info.php`, `actor_info.php`, `tools.php` et `tab.css`

1. La réponse est au format JSON. Il s'agit du film Sonic le film. Le paramètre `language=fr` permet d'afficher le résultat en langue française, cela est visible notamment dans la description du film.

2. La commande `curl http://api.themoviedb.org/3/movie/454626?api_key=...&language=fr` permet d'obtenir la réponse en JSON. La fonction `smartcurl()` renvoie la réponse dans un tableau.

3. Nous avons utilisé la fonction `file_get_contents()` pour obtenir le fichier JSON depuis la requête vers l'API de TMDB. Puis la fonction `json_decode()` nous a permis de le décoder pour obtenir un tableau associatif. Il est donc finalement facile de retrouver les informations dans ce tableau et de les afficher.


### Les choses sérieuses

4. On ajoute les URL correspondant aux versions anglaises et originales. On affiche ensuite les informations dans un tableau HTML classique, avec chaque ligne correspondant à une information pour les trois versions.

5. L'URL des affiches de l'API est de la forme `http://image.tmdb.org/t/p/w185`, auquel il nous suffit de rajouter le champ `poster_path`. Grâce à la balise `<a>`, les affiches sont cliquables et mènent à la page TMDB du film correspondant.

6. Grâce à l'URL `search/collection?query=The+Lord+of+the+Rings` on obtient le résultat pour la recherche `Lord of the Rings` parmi les collections de TMDB. Elle contient un champ `results` qui contient lui-même des champs numérotés pour les différents résultats. On peut repérer que la première collection correspond à la trilogie que nous recherchons. On peut alors utiliser l'URL `collection/id_collection` pour récupérer la liste de tous les films de cette collection, dans le champ `parts`, et les traiter un par un.

7. Pour chaque film, on effectue une requête sur son `ID`, avec un URL de la forme `movie/movie_id/credits` afin de récupérer ses crédits. On sélectionne le champ `actors` qui correspond à son casting. On peut alors parcourir la liste des acteurs, et les ajouter à une liste, en incrémentant leur nombre d'occurence s'ils y sont déjà (cela grâce à la fonction `array_key_exists()`). Enfin, on effectue, avant l'affichage, un tri de la liste sur leur `cast_id` grâce à la fonction `ksort()`.

8. Le champ `character` contient, pour les figurants, un terme générique, comme "soldat" ou "hobbit". On pourrait donc l'utiliser, mais cela ne fonctionne pas pour les personnages nommés, comme "Frodon". On ne peut donc pas récupérer la liste de tous les hobbits.

9. Nous avons choisi d'ajouter un lien sur l'ID de chaque acteur. Ce lien renvoie vers la page `actor_info.php`, avec un paramètre `id` correspondant à l'ID de l'acteur en question. L'URL `/person/actor_id/movie_credits` permet de récupérer la liste des films dans lesquels il a joué, contenus dans le champs `cast`. Le nom du film ainsi que le rôle sont directement donnés, cet affichage est donc plutôt facile. De plus cliquer sur le nom de l'acteur renvoie sur la page TMDB lui correspondant.

10. On utilise l'URL `movie/movie_id/videos` pour récupérer la liste des vidéos associées à un film, dans le champs `results`. Nous prenons pas défaut la première. Ensuite, le champ `key` contient l'adresse de la vidéo sur YouTube. Il suffit alors d'ajouter un élément `<iframe>` ayant pour source `https://www.youtube.com/embed/key`. 