<style>
  <?php include "tab.css" ?>
</style>


<?php
  require_once("../../Helpers/tp3-helpers.php");
  require_once("tools.php");


  // --- Données ---
  $sonic_vo = query_tmdb($url_sonic);
  $sonic_fr = query_tmdb($url_sonic, ["language" => "fr"]);
  $sonic_en = query_tmdb($url_sonic, ["language" => "en"]);


  // --- 2 ---
  echo_question(2, "JSON brut");

  $json_brut = smartcurl($url_sonic);
  echo "<i>L'affichage a été masqué dans le code.</i><br>";
  //print_r($json_brut);


  // --- 3 ---
  echo_question(3, "Informations sur le film");

  echo "Titre original : <a href='" . $sonic_fr['homepage'] . "'>" . $sonic_fr["original_title"] . "</a><br>";
  echo "Titre : " . $sonic_fr["title"] . "<br>";
  echo "Accroche : " . $sonic_fr["tagline"] . "<br>";
  echo "Résumé : " . $sonic_fr["overview"] . "<br>";


  // --- 4, 5 ---
  echo_question("4, 5", "Affichage en trois colonnes");

  // Tableaux contenant les informations à afficher
  $titles = ["Affiche", "Titre original", "Titre", "Accroche", "Résumé"];
  $contents = ["poster_path", "original_title", "title", "tagline", "overview"];

  // Affichage du tableau
  echo_table_header(["", "VO", "VF", "VA"]);
  // Affichage des affiches avec un lien vers la page TMDB
  echo_table_row(
    [$titles[0],
    "<a href='" . $url_tmdb . $url_movie . $sonic_vo["id"] . "'><img src='" . $url_poster . $sonic_vo[$contents[0]] . $api_key . "' alt='Poster_Movie'></a>",
    "<a href='" . $url_tmdb . $url_movie . $sonic_fr["id"] . "'><img src='" . $url_poster . $sonic_fr[$contents[0]] . $api_key . "' alt='Poster_Movie'></a>",
    "<a href='" . $url_tmdb . $url_movie . $sonic_en["id"] . "'><img src='" . $url_poster . $sonic_en[$contents[0]] . $api_key . "' alt='Poster_Movie'></a>"],
    TRUE);
  // Affichage des autres informations
  for ($i = 1; $i < count($contents); $i++)
    echo_table_row([$titles[$i], $sonic_vo[$contents[$i]], $sonic_fr[$contents[$i]], $sonic_en[$contents[$i]]], TRUE);
  echo_table_footer();


  // --- 6, 10 ---
  echo_question("6, 10", "Recherche sur Le Seigneur des Anneaux");

  $lotr_collec = query_tmdb($url_search_collection, ["query" => "The+Lord+of+the+Rings"])["results"];
  $lotr = query_tmdb($url_collection . $lotr_collec[0]["id"])["parts"];

  // Affichage des informations et de la bande-annonce pour chaque film
  echo_table_header(["ID", "Titre", "Date de sortie", "Bande-annonce"]);
  foreach ($lotr as $movie)
  {
    $video = query_tmdb($url_movie . $movie["id"] . "/videos")["results"];
    echo_table_row([$movie["id"], $movie["title"], $movie["release_date"],
                    isset($video[0]) ? "<iframe width='360' height='240' src='https://www.youtube.com/embed/". $video[0]["key"] . "'></iframe>" : "Aucune bande-annonce disponible."]);
  }
  echo_table_footer();


  // --- 7, 9 ---
  echo_question("7, 9", "Liste des acteurs");

  $list_actors = []; // Liste des acteurs

  // Pour chaque film trouvé lors de la recherche
  foreach ($lotr as $movie) {
    // Recherche de la liste de ses acteurs
    $actors = query_tmdb($url_movie . $movie["id"] . "/credits")["cast"];

    // Si l'acteur est déjà présent la liste, on augmente son compteur, sinon on l'ajoute
    foreach ($actors as $actor) {
      if (array_key_exists($actor["id"], $list_actors))
        $list_actors[$actor["id"]][2]++;
      else
        $list_actors[$actor["id"]] = [$actor["name"], $actor["character"], 1];
    }
  }

  ksort($list_actors); // Tri des acteurs par leur ID

  // Affichage des acteurs
  echo_table_header(["ID", "Nom", "Personnage", "Nombre d'occurences"]);
  foreach ($list_actors as $id => $actor)
  {
    echo_table_row(["<a href='actor_info.php?id=" . $id . "&name=". $actor[1] . "'>" . $id . "</a>", $actor[0], $actor[1], $actor[2]]);
  }
  echo_table_footer();
?>
