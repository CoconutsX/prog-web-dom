<!-- Fonctions -->

<?php
  // --- URLs ---
  $api_key = "?api_key=ebb02613ce5a2ae58fde00f4db95a9c1";

  $url_tmdb = "https://www.themoviedb.org/";
  $url_poster = "http://image.tmdb.org/t/p/w185";

  $url_movie = "movie/";
  $url_collection = "collection/";
  $url_personn = "person/";

  $url_search_movie = "search/movie";
  $url_search_collection = "search/collection";

  $url_sonic = "movie/454626";

  
  // Renvoie la réponse décodée d'une requête vers l'API de TMDB
  function query_tmdb($urlcomponent, $params = null)
  {
    return json_decode(tmdbget($urlcomponent, $params), TRUE);
  }

  // Affiche l'en-tête d'une table
  function echo_table_header($titles)
  {
    echo "<table><thead>";
    foreach ($titles as $title)
      echo "<th>" . $title . "</th>";
    echo "</thead><tbody>";
  }

  // Affiche une ligne d'une table
  function echo_table_row($cells, $double_entry = FALSE)
  {
    echo "<tr>";
    // Si c'est un tableau a double-entrée, afficher la première case comme un titre et la supprimer
    if($double_entry)
    {
      echo "<th>" . $cells[0] . "</th>";
      unset($cells[0]);
    }
    foreach ($cells as $cell)
      echo "<td>" . $cell . "</td>";
    echo "</tr>";
  }

  // Affiche le pied-de-page d'une table
  function echo_table_footer()
  {
    echo "</tbody></table>";
  }

  // Affiche l'intitulé de la question
  function echo_question($number, $title)
  {
    echo "<br><h3>" . $number . ") " . $title . " :</h3><br>";
  }
?>
