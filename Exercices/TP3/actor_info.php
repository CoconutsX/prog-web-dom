<style>
  <?php include "tab.css" ?>
</style>


<?php
  require_once("../../Helpers/tp3-helpers.php");
  require_once("tools.php");


  // --- 9 ---
  $id = isset($_GET["id"]) ? $_GET["id"] : NULL;
  $name = isset($_GET["name"]) ? $_GET["name"] : NULL;
  
  if($id != null && $name != NULL)
  {
    echo "<u>Acteur :</u> <a href='". $url_tmdb . $url_personn . $id . "'>" . $name . " (" . $id . ")</a><br><br>";

    $movies = query_tmdb($url_personn . $id . "/movie_credits")["cast"];

    echo_table_header(["ID", "Titre", "Rôle"]);
    foreach($movies as $movie)
      echo_table_row([$movie["id"], $movie["title"], $movie["character"]]);
    echo_table_footer();
  }

?>